import smtplib
import time
import imaplib
import email
def get_text(msg):
    if msg.is_multipart():
        return get_text(msg.get_payload(0))
    else:
        return msg.get_payload(None, True)

def read_email_from_gmail(_date):
    '''    returns top 5 most mentioned symbols from all emails
    and top 1 symbol mentioned in each email that passes a rule'''
    try:
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL,FROM_PWD)
        mail.select('inbox')

        type, data = mail.search(None,'(SINCE "'+ (_date).strftime("%d-%b-%Y") +'")')
        mail_ids = data[0]

        id_list = mail_ids.split()
        first_email_id = int(id_list[0])
        latest_email_id = int(id_list[-1])

        all_bodies = []
        top_symbols = []
        for i in range(latest_email_id,first_email_id, -1):
            typ, data = mail.fetch(i, '(RFC822)' )
            for response_part in data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    (body, top_symbol)=email_filetr(msg)
                    all_bodies.append(body)
                    if(len(top_symbol)>0):
                        top_symbols.append(top_symbol)
        return(check_for_top_symbols(all_bodies, 100),top_symbols)


    except Exception, e:
        print str(e)
