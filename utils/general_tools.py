from collections import Counter
import requests
import json
from utils.constants import *

def multiply_on_zero(val1, val2):
    if((isinstance(val1, int) or isinstance(val1, float)) and (isinstance(val2, int) or isinstance(val2, float))):
        return val1 * val2
    else:
        return 0

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

import re
def find_between(flt, txt):
    result = re.search(flt, txt)
    try:
        return result.group(1)
    except:
        return None


def get_symbol(txt):
    regex = re.compile('[^a-zA-Z]')
    filtered = regex.sub('', txt)
    if(filtered.isupper() and len(filtered)>2 and \
       len(filtered)<6 and not hasNumbers(filtered) and \
       filtered not in not_symbol):
        return filtered
    else:
        return None


def check_for_rules(email_subject,email_from,body):
    for i in range(len(rules)):
        rule=rules[i]
        if(email_from == rule['from'] and \
           rule['in_subject'] in email_subject and \
           rule['in_body'] in body):
            return (True, i)
    return (False, -1)


def check_for_top_symbols(all_bodies, top=10):
    all_symbols = []
    for body in all_bodies:
        for line in body.split("\n"):
            for word in line.split(" "):
                symbol =  get_symbol(word)
                if symbol is not None:
                    all_symbols.append(symbol)
    tops = Counter(all_symbols).most_common(5)
    most_common = [x[0] for x in tops]
    return most_common

def email_filetr(msg):
    if msg.is_multipart():
        body = get_text(msg.get_payload(0))
    else:
        body = msg.get_payload(None, True)
    email_subject = msg['subject']
    result = re.search('<(.*)>', msg['from'])
    email_from = result.group(1)
    (found, rule_number)=check_for_rules(email_subject,email_from,body)
    symbols=[]
    if(found):
        rule = rules[rule_number]
        fn = rule['get_symb']
        symbols=fn(body)
    top_symbol=""
    if(len(symbols)>0):
        top_symbol = Counter(symbols).most_common(1)[0][0]
    return (body, top_symbol)

def find_key_in_json(text,key,path):
    found = False
    for k in text:
        if(found):
            break
        if k==key:
            print(path+"/"+key)
            return True
        else:
            _result = text[k]
            if(isinstance(_result, dict)):
                found= find_key_in_json(_result,key,path+"/"+k)
    return found
