import tweepy
from utils.constants import *
from utils.general_tools import *

def get_tweet_texts(user,count,max_id=None):
    if(max_id is None):
        result=api.user_timeline(user,count=count, include_rts=True)
    else:
        result=api.user_timeline(user,count=count, include_rts=True, max_id=max_id)
    texts=[]
    ids = []
    dates = []
    for i in range(len(result)):
        texts.append(result[i].text)
        ids.append(result[i].id)
        dates.append(result[i].created_at)
    return (texts, ids, dates)

def get_twitter_symbols():
    twitter_symbols_account = []
    twitter_symbols = []
    for account in twitter_accounts:
        (tweets,ids,dates) = get_tweet_texts(account, 50)
        tweet_symbols = filter(lambda x: x is not None, [get_symbol(word) for words in [tweet_text.split(" ") for tweet_text in tweets] for word in words])
        filtered_symbols = []
        for symb in tweet_symbols:
            if(symb not in twitter_symbols):
                twitter_symbols.append(symb)
                filtered_symbols.append(symb)
        twitter_symbols_account.append({"account": account,"symbols": filtered_symbols})
    return twitter_symbols_account
