import requests
import json
import datetime
import pandas_datareader.data as data
import pandas_datareader.data as web
from utils.general_tools import multiply_on_zero
import pandas_datareader as pdr

def check_market_cap(data, low, high):
    market_cap = data['context']['dispatcher']['stores']['QuoteSummaryStore']['summaryDetail']['marketCap']['raw']
    if(market_cap>high or market_cap< low):
        return True
    else:
        return False

def check_for_buy(symbol):
    two_month_ago_date = (datetime.date.today()-datetime.timedelta(days=65)).strftime("%d/%m/%Y")
    today_date = datetime.date.today().strftime("%d/%m/%Y")
    try:
        df_ = get_data(symbol,today_date, two_month_ago_date)
        illiquid = check_liquidity(df_, 60, 7, 100000) #past 60 days, moving average per 7 days, not any less that 100,000 share volume
        high_price = check_price(df_, 3, 10)#past 3 days any more than $10 price
    except:
        illiquid = True
        high_price = True

    try:
        data = get_data(symbol) #get finance data
        bad_market_cap = check_market_cap(data, 10000000, 500000000)#less than 10 million and more than 500 million market cap
    except:
        bad_market_cap = True

    return (not illiquid and not high_price and not bad_market_cap)


def check_price(df_, days_to_check, threshold):
    closing_price = df_['Close'].tail(days_to_check)
    return (closing_price>threshold).any()

def check_for_buy_fast(symbol, vol_threshold, price_threshold):
    start = datetime.date.today() - datetime.timedelta(days=1)
    end = start + datetime.timedelta(days=1)
    df = get_data()
    illiquid = (df_.ix[str(start)]['Volume']<vol_threshold)
    high_price = (df_.ix[str(start)]['Close']>price_threshold)
    return (not illiquid and not high_price)

def check_liquidity(df_, days_to_check, window, threshold):
    vol_moving_avg = df_['Volume'].rolling(window=window).mean().tail(days_to_check)
    return (vol_moving_avg<threshold).any()

def find_pump_dump(_df, up_perct=0.3):
    # FIXME: instead of range compare average
    _range = _df['52-week Range'].split(" - ")
    _low = float(_range[0])
    _high = float(_range[1])
    _average = _df['50-day Moving Average']
    if(_high > _average * (1 + up_perct)):
        return True
    else:
        return False

def check_for_candid(list_of_symbols, ave_vol_threshold, price_threshold_up, market_cap_lb, market_cap_ub, liquid_ave_threshold, last_day_vol_threshold, last_date_day_threshold, price_threshold_low):
    data_ = get_stock_stats(list_of_symbols)
    volume_ave_list = [x['Avg Daily Volume'] > ave_vol_threshold for x in json.loads(data_.to_json(orient='records'))]
    volume_last_list = [x['Volume'] > last_day_vol_threshold for x in json.loads(data_.to_json(orient='records'))]
    liquid_ave_list = [multiply_on_zero(x['Avg Daily Volume'], x['200-day Moving Average']) > liquid_ave_threshold for x in json.loads(data_.to_json(orient='records'))]
    low_price = [(x['Prev Close'] < price_threshold_up and x['Prev Close'] > price_threshold_low) for x in json.loads(data_.to_json(orient='records'))]
    good_market_cap = [(x['Market Cap'] > market_cap_lb) for x in json.loads(data_.to_json(orient='records'))] # and x['Market Cap'] < market_cap_ub
    last_date = [datetime.datetime.strptime(x['Last Trade Date'], '%m/%d/%Y') >= (datetime.datetime.now() - datetime.timedelta(days=last_date_day_threshold))  for x in json.loads(data_.to_json(orient='records'))]
    for x in json.loads(data_.to_json(orient='records')):
        if(find_pump_dump(x)):
            print("find pump dump: " + x['Symbol'])
    return [volume_last_list[i] and volume_ave_list[i] and liquid_ave_list[i] and low_price[i] and good_market_cap[i] and last_date[i] for i in range(len(list_of_symbols))]


    _data = get_data(stocklist)
    return _data
