import itertools
import tweepy

## Common non-symbol words which are like symbols!
not_symbol = ['FULL', 'TONS', 'CEO', 'USD', 'THE', 'CENT', 'FIVE', 'EOD', 'YEAR', 'AND', 'TOP', 'HARD', 'OUT', 'SOLD',
              'MARK', 'ISO', 'HERE', 'FOR', 'REAL', 'LLC', 'HUGE', 'PINK', 'GDP', 'MARK', 'OTC', 'NOT', 'MISS', 'EST',
              'BIG', 'LOW', 'VERY', 'PLC', 'THIS', 'YOU', 'USA', 'NEAR','NEW','HIGH','MEGA','SMS','NOW', 'SEC', 'RAW',
              'HOT']

rules=[
    {"from":'editor@pennypickalerts.com',
     "in_subject":"Hot Streak Alert",
     "in_body": "Today's Play",
     "get_symb": lambda x: filter(lambda symbol: symbol is not None, map(lambda word: get_symbol(word) , list(itertools.chain(*map(lambda line: line.split(" "), x.split("\n"))))))
    }
]

ORG_EMAIL   = "@gmail.com"
FROM_EMAIL  = "****" + ORG_EMAIL
FROM_PWD    = "****"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

auth = tweepy.OAuthHandler("******", "*******")
auth.set_access_token("******", "*******")
api = tweepy.API(auth)

twitter_accounts = ['RatingsNetwork', 'ProVesting', 'JorelLaraKalel', 'ZtradeCZAR', 'TheMarketsDaily', 'ChosenTrades',
                    'intercooleronli', 'WeekHerald', 'storminvestor', 'Allstocknews', 'rcachap', 'shaneblackmon', 'COCKRELLGROUP',
                    'DrugDeliveryNow', 'ZacharyBrennan', 'BioRunUp', 'jbs_pennystocks', 'Follow_Trades', 'investorslive', 'codytrades',
                    'geckler', 'alphatrends', 'docking', 'super_trades', 'adamfeuerstein', 'toddsullivan', 'SmCapMarketPl']
