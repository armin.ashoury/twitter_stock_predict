from utils.twitter_tools import get_tweet_texts
from utils.general_tools import get_symbol
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter
import pandas_datareader as pdr
import datetime
import time
from utils.constants import twitter_accounts

symbol='$AAGIY'
max_count = 200
needed_count = 2000
max_id = -1
results = []
all_symbols = []
for account in twitter_accounts:
    max_id = -1
    for i in range(needed_count/max_count):
        if(max_id==-1):
            (texts,ids, dates) = get_tweet_texts(account,max_count)
        else:
            (texts,ids, dates) = get_tweet_texts(account,max_count,max_id)
        if(len(ids)>0):
            max_id = min(ids)
        for i in range(len(texts)):
            tweet_symbols = filter(lambda x: x is not None, [get_symbol(word) for words in [tweet_text.split(" ") for tweet_text in texts[i].split("\n")] for word in words])
            results.append({"texts":texts[i],"dates": dates[i], "symbols": tweet_symbols})
            for symbol in tweet_symbols:
                all_symbols.append({"date": dates[i], "symbol": symbol})


letter_counts = Counter([x['symbol'] for x in all_symbols])
df = pd.DataFrame.from_dict(letter_counts, orient='index',)
df = df.rename(index=str, columns={0: "symbols"})
selected_df = df.sort_values('symbols', ascending = False).head()
selected_df.plot(kind='bar')
plt.show()
print(selected_df)

symbol = 'CHRO'
month_ago_date = "2017-08-01"#(datetime.datetime.now()-datetime.timedelta(days=30)).strftime("%Y-%m-%d")
today_date = datetime.datetime.now().strftime("%Y-%m-%d")

fig, axes = plt.subplots(nrows=2, ncols=1)

df = pd.DataFrame(all_symbols)
df['date'] = pd.to_datetime(df.date).dt.date
df[(df.symbol==symbol)].groupby('date').agg('count').plot(ax=axes[0])

df_prices = get_data(symbol)
filtered_df_prices = df_prices.ix[month_ago_date:today_date]

print(filtered_df_prices)
price_ = filtered_df_prices['Close']
price_.plot(ax=axes[1])
plt.show()
