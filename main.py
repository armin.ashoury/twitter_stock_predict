import datetime
from utils.stock_tools import *
from utils.twitter_tools import *

symbols_file = "***/OTCBB_20170911.csv"
group_size = 10

l = open(symbols_file, "r")
results = []
for line in l:
    objects_ = line.split(",")
    if(objects_[0] == 'Symbol'):
        continue
    results.append({'Symbol': objects_[0].split(".OB")[0],
                    'Date': objects_[1],
                    'Open': float(objects_[2]),
                    'High': float(objects_[3]),
                     'Low': float(objects_[4]),
                     'Close': float(objects_[5]),
                     'Volume': int(objects_[6].split("\n")[0])
                    })
symbols = []
for obj in results:
    symbols.append(obj['Symbol'])



for i in range(len(symbols)/group_size):
    print("{0} out of {1}".format(i+1, len(symbols)/group_size))
    selected_symbols = symbols[i * group_size:min(len(symbols), group_size * (i + 1))]
    buys = check_for_candid(list_of_symbols=selected_symbols, ave_vol_threshold=100000, price_threshold_up=10,
                                market_cap_lb=10000000, market_cap_ub=None, liquid_ave_threshold = 10000*0.5,
                                last_day_vol_threshold=10000, last_date_day_threshold=5, price_threshold_low=0.1)
    for j in range(len(buys)):
        if(buys[j]):
            print(symbols[j] + " : Buy")
        else:
            print(symbols[j] + " : Don't Buy")
    break
    if(i>2):
        break
